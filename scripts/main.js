'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var myApp = {
	// сет брейкпоинтов для js
	// должны совпадать с теми что в body:after
	mediaBreakpoint: {
		sm: 576,
		md: 768,
		lg: 1024,
		xl: 1281,
		xxl: 1600
	},
	init: function init() {
		jQuery.extend(jQuery.validator.messages, {
			required: "Это поле обязательое",
			remote: "Пожалуйста, исправьте это поле",
			email: "Пожалуйста, введите действительный адрес электронной почты."
		});

		this.lazyload();

		if ('objectFit' in document.documentElement.style === false) {
			this.objectFitFallback($('[data-object-fit]'));
		}

		$('.js-tel').mask('+7 (999) 999-99-99', {
			placeholder: '+7 (___) ___-__-__'
		});

		this.formValidate($(".js-validate"));
		this.buttons();
		this.videoUpload();
		this.animateCss();
		this.modals();
		this.closeOnFocusLost();
		this.particles();
		this.backgroundWidth();
		this.testIe();
	},

	initOnLoad: function initOnLoad() {
		this.scrollMagic();
		this.preloader();
		setTimeout(function () {
			AOS.init({
				easing: 'ease-in-sine',
				duration: 1000
			});
		}, 500);
	},

	testIe: function testIe() {
		if (/MSIE 10/i.test(navigator.userAgent)) {
			return true;
		}

		if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent)) {
			return true;
		}

		if (/Edge\/\d./i.test(navigator.userAgent)) {
			return true;
		}
	},

	preloader: function preloader() {
		setTimeout(function () {
			$('.vf-preloader').removeClass('vf-preloader--active');
			$('.vf-content').removeClass('vf-content--not-loaded');
		}, 0);
	},

	particles: function particles() {
		if (this.testIe()) {
			return false;
		}

		particlesJS("particles-js", {
			"particles": {
				"number": {
					"value": 60,
					"density": {
						"enable": true,
						"value_area": 800
					}
				},
				"color": {
					"value": "#ffffff"
				},
				"shape": {
					"type": "circle",
					"stroke": {
						"width": 0,
						"color": "#000000"
					},
					"polygon": {
						"nb_sides": 5
					},
					"image": {
						"src": "img/github.svg",
						"width": 100,
						"height": 100
					}
				},
				"opacity": {
					"value": 1,
					"random": true,
					"anim": {
						"enable": true,
						"speed": 1,
						"opacity_min": 0,
						"sync": false
					}
				},
				"size": {
					"value": 3,
					"random": true,
					"anim": {
						"enable": false,
						"speed": 4,
						"size_min": 0.3,
						"sync": false
					}
				},
				"line_linked": {
					"enable": false,
					"distance": 150,
					"color": "#ffffff",
					"opacity": 0.4,
					"width": 1
				},
				"move": {
					"enable": true,
					"speed": 1,
					"direction": "none",
					"random": true,
					"straight": false,
					"out_mode": "out",
					"bounce": false,
					"attract": {
						"enable": false,
						"rotateX": 600,
						"rotateY": 600
					}
				}
			},
			"interactivity": {
				"detect_on": "window",
				"events": {
					"onhover": {
						"enable": true,
						"mode": "bubble"
					},
					"onclick": {
						"enable": true,
						"mode": "repulse"
					},
					"resize": true
				},
				"modes": {
					"grab": {
						"distance": 400,
						"line_linked": {
							"opacity": 1
						}
					},
					"bubble": {
						"distance": 250,
						"size": 0,
						"duration": 2,
						"opacity": 0,
						"speed": 3
					},
					"repulse": {
						"distance": 400,
						"duration": 0.4
					},
					"push": {
						"particles_nb": 4
					},
					"remove": {
						"particles_nb": 2
					}
				}
			},
			"retina_detect": true
		});
	},

	backgroundWidth: function backgroundWidth() {
		var $bg = $('.vf-bg');
		var $content = $('.vf-content');
		var trottle = void 0;

		$bg.width($content.width());

		$(window).resize(function () {
			clearTimeout(trottle);
			trottle = setTimeout(function () {
				$bg.width($content.width());
			}, 100);
		});
	},

	animateCss: function animateCss() {
		$.fn.extend({
			animateCss: function animateCss(animationName, callback) {
				var animationEnd = function (el) {
					var animations = {
						animation: 'animationend',
						OAnimation: 'oAnimationEnd',
						MozAnimation: 'mozAnimationEnd',
						WebkitAnimation: 'webkitAnimationEnd'
					};

					for (var t in animations) {
						if (el.style[t] !== undefined) {
							return animations[t];
						}
					}
				}(document.createElement('div'));

				this.addClass('animated ' + animationName).one(animationEnd, function () {
					$(this).removeClass('animated ' + animationName);

					if (typeof callback === 'function') callback();
				});

				return this;
			}
		});
	},

	fixBg: function fixBg() {
		var $bg = $('.vf-bg');
		var $footer = $('.vf-footer');
		var bgHeight = $bg.outerHeight();
		var footerOffset = $footer.offset().top;
		var footerHeight = $footer.outerHeight();
		var currentPos = document.documentElement.scrollTop;
		var windowHeight = window.innerHeight;
		var sidebarWidth = $('#vw_page_header nav').outerWidth();

		var fixBackground = function fixBackground() {
			if (currentPos + windowHeight >= bgHeight) {
				if (currentPos + windowHeight >= footerOffset) {
					$bg.removeClass('vf-bg--fixed');
					$bg.addClass('vf-bg--footer-reached');
					$bg.css({
						bottom: footerHeight - 1
					});
				} else {
					$bg.removeClass('vf-bg--footer-reached');
					$bg.addClass('vf-bg--fixed');
					$bg.css('bottom', '');
				}
			} else {
				$bg.removeClass('vf-bg--fixed');
			}
		};

		fixBackground();

		$(window).scroll(function () {
			bgHeight = $bg.outerHeight();
			footerOffset = $footer.offset().top;
			currentPos = document.documentElement.scrollTop;
			fixBackground();
		});
	},

	scrollMagic: function scrollMagic() {
		var app = this;
		var controller = new ScrollMagic.Controller();
		var bg = document.querySelector('.vf-bg');
		var sections = document.querySelectorAll('.vf-section');
		var wow = document.querySelectorAll('.wow');
		var scenes = {};

		// Фиксация основного фона при скролле
		if (bg !== null) {
			if (this.testIe()) {
				this.fixBg();
			} else {
				var duration = $('body').outerHeight() - bg.offsetHeight - $('.vf-footer').outerHeight();
				scenes.pinBg = new ScrollMagic.Scene({
					triggerElement: bg,
					offset: bg.offsetHeight,
					triggerHook: 1,
					duration: duration
				}).setPin(bg)
				// .setClassToggle(bg, "vf-bg--fixed")
				// .addIndicators() // add indicators (requires plugin)
				.addTo(controller);
			}
		}

		// боковая пагинация
		if (sections !== null) {
			var pagingItems = document.querySelectorAll('.vf-paging__item');
			for (var i = 0; i < sections.length; i++) {
				scenes.sectionSpy = new ScrollMagic.Scene({
					triggerElement: sections[i],
					duration: sections[i].offsetHeight
				}).setClassToggle(pagingItems[i], "active")
				// .addIndicators() // add indicators (requires plugin)
				.addTo(controller);
			}
		}
	},

	buttons: function buttons() {
		$(document).on('click', '.menu-trigger', function () {
			$('body').toggleClass('nav-showed');
		});

		// paging listener
		$(document).on('click', '.vf-paging__link', function (e) {
			e.preventDefault();
			var href = $(this).attr('href');
			if ($(href).length) {
				$('html, body').stop(true, false).animate({
					scrollTop: $(href).offset().top
				}, 1000);
			}
		});
	},

	closeOnFocusLost: function closeOnFocusLost() {
		$(document).click(function (e) {
			var $trg = $(e.target);
			if (!$trg.closest(".header").length) {
				$('body').removeClass('nav-showed');
			}
		});
	},

	lazyload: function (_lazyload) {
		function lazyload() {
			return _lazyload.apply(this, arguments);
		}

		lazyload.toString = function () {
			return _lazyload.toString();
		};

		return lazyload;
	}(function () {
		try {
			lazyload();
		} catch (e) {
			console.log('lazyload fallback');
			$('.lazyload').each(function () {
				var $t = $(this);
				var src = $(this).attr('src');
				var dataSrc = $t.data().src;
				if ((typeof src === "undefined" ? "undefined" : _typeof(src)) !== (typeof undefined === "undefined" ? "undefined" : _typeof(undefined)) && src !== false && $t.is('img')) {
					$t.attr('src', dataSrc);
				} else {
					$t.css({
						'background-image': 'url(' + dataSrc + ')'
					});
				}
			});
		}
	}),

	formValidate: function formValidate($el) {
		var app = this;
		$el.each(function () {
			var $form = $(this);
			$form.validate({
				// errorLabelContainer: $('.form-error', $form),
				// wrapper: 'p',
				errorElement: 'span',
				highlight: function highlight(element) {
					$(element).addClass("error").parent().addClass("error");
				},
				unhighlight: function unhighlight(element) {
					$(element).removeClass("error").parent().removeClass("error");
				},
				submitHandler: function submitHandler() {
					alert('отправка');
				}
			});
		});
	},

	modalsDefault: {
		type: 'inline',
		removalDelay: 300,
		mainClass: 'mfp-fade',
		closeBtnInside: false,
		prependTo: '.vf-content',
		callbacks: {
			open: function open() {
				$('html').addClass('modal-opened');
			},
			close: function close() {
				$('html').removeClass('modal-opened');
			}
		}
	},

	modals: function modals() {
		var app = this;
		$('.js-inline-modal').magnificPopup(app.modalsDefault);
		var modalsCustom = app.modalsDefault;
		modalsCustom.items = {
			src: $('#modal-rules'),
			type: 'inline'
			// $.magnificPopup.open(modalsCustom);

		};$(document).on('click', '.js-modal-close', function (e) {
			e.preventDefault();
			$.magnificPopup.close();
		});
	},

	videoUpload: function videoUpload() {
		var app = this;

		// feature detection for drag&drop upload
		var isAdvancedUpload = function () {
			var div = document.createElement('div');
			return ('draggable' in div || 'ondragstart' in div && 'ondrop' in div) && 'FormData' in window && 'FileReader' in window;
		}();

		// applying the effect for every form

		$('.vf-upload').each(function () {
			var $box = $(this);
			var $form = $box.find('form');
			var $input = $form.find('input[type="file"]');
			var $label = $form.find('label');
			var $errorMsg = $form.find('.vf-upload__error span');
			var $restart = $form.find('.vf-upload__restart');
			var $actionsBar = $box.find('.vf-upload__actions');
			var $remove = $box.find('.vf-upload__remove');
			var $playBtn = $box.find('.vf-upload__video-btn');
			var maxSize = $input.data('max-size');
			var droppedFiles = false;
			var video = void 0;

			// показать выбранное видео
			var showFiles = function showFiles(files) {
				if (files.length) {
					var file = files[0];
					var fileSizeMb = file.size / 1024 / 1024;
					var fileReader = new FileReader();
					if (file.type.match('video') && maxSize > fileSizeMb) {
						$actionsBar.fadeIn(200);
						fileReader.onload = function () {
							var blob = new Blob([fileReader.result], { type: file.type });
							var url = URL.createObjectURL(blob);
							video = document.createElement('video');

							video.src = url;
							$form.find('video').remove();
							$form.append(video);
							$playBtn.fadeIn(200);

							video.onpause = function () {
								$playBtn.show(0);
								video.controls = false;
							};
						};
						fileReader.readAsArrayBuffer(file);
					} else {
						$form.addClass('is-error');
						$errorMsg.text('Поддерживаются только видео файлы размером не более ' + maxSize + 'Mb');
					}
				}
			};

			// letting the server side to know we are going to make an Ajax request
			$form.append('<input type="hidden" name="ajax" value="1" />');

			// automatically submit the form on file select
			$input.on('change', function (e) {
				showFiles(e.target.files);
			});

			// при удалении видео
			$remove.on('click', function (e) {
				e.preventDefault();
				$input[0].value = null;
				$actionsBar.fadeOut(200);
				$form.find('video')[0].remove();
				setTimeout(function () {
					$playBtn.hide(0);
				}, 50);
			});

			// кастомная кнопка запуска видео
			$playBtn.on('click', function (e) {
				video.controls = true;
				video.play();
				$playBtn.hide(0);
			});

			// drag&drop files if the feature is available
			if (isAdvancedUpload) {
				$form.addClass('has-advanced-upload') // letting the CSS part to know drag&drop is supported by the browser
				.on('drag dragstart dragend dragover dragenter dragleave drop', function (e) {
					// preventing the unwanted behaviours
					e.preventDefault();
					e.stopPropagation();
				}).on('dragover dragenter', function () //
				{
					$form.addClass('is-dragover');
				}).on('dragleave dragend drop', function () {
					$form.removeClass('is-dragover');
				}).on('drop', function (e) {
					droppedFiles = e.originalEvent.dataTransfer.files; // the files that were dropped
					showFiles(droppedFiles);
				});
			}

			// if the form was submitted

			$form.on('submit', function (e) {
				// preventing the duplicate submissions if the current one is in progress
				if ($form.hasClass('is-uploading')) return false;

				$form.addClass('is-uploading').removeClass('is-error');

				if (isAdvancedUpload) // ajax file upload for modern browsers
					{
						e.preventDefault();

						// gathering the form data
						var ajaxData = new FormData($form.get(0));
						if (droppedFiles) {
							$.each(droppedFiles, function (i, file) {
								ajaxData.append($input.attr('name'), file);
							});
						}

						// ajax request
						/*$.ajax({
      	url: $form.attr('action'),
      	type: $form.attr('method'),
      	data: ajaxData,
      	dataType: 'json',
      	cache: false,
      	contentType: false,
      	processData: false,
      	complete: function() {
      		// $form.removeClass('is-uploading');
      	},
      	success: function(data) {
      		$form.removeClass('is-error is-success is-uploading');
      		if (!data.success) $errorMsg.text(data.error);
      		$remove.click();
      				const modalsCustom = app.modalsDefault;
      		modalsCustom.items = {
      				src: $('#modal-uploaded'),
      				type: 'inline'
      		}
      		$.magnificPopup.open(modalsCustom);
      	},
      	error: function() {
      		// alert('Ошибка. Пожалуйста, свяжитесь с веб-мастером!');
      	}
      });*/

						$form.removeClass('is-error is-success is-uploading');
						$remove.click();

						var modalsCustom = app.modalsDefault;
						modalsCustom.items = {
							src: $('#modal-uploaded'),
							type: 'inline'
						};
						$.magnificPopup.open(modalsCustom);
					} else // fallback Ajax solution upload for older browsers
					{
						var iframeName = 'uploadiframe' + new Date().getTime(),
						    $iframe = $('<iframe name="' + iframeName + '" style="display: none;"></iframe>');

						$('body').append($iframe);
						$form.attr('target', iframeName);

						$iframe.one('load', function () {
							var data = $.parseJSON($iframe.contents().find('body').text());
							$form.removeClass('is-uploading').addClass(data.success == true ? 'is-success' : 'is-error').removeAttr('target');
							if (!data.success) $errorMsg.text(data.error);
							$iframe.remove();
						});
					}
			});

			// restart the form if has a state of error/success

			$restart.on('click', function (e) {
				e.preventDefault();
				$form.removeClass('is-error is-success');
				$input.trigger('click');
			});
		});
	},

	objectFitFallback: function objectFitFallback($selector) {
		$selector.each(function (i, item) {
			var $t = $(item);
			var imgUrl = $t.attr('src');
			var fitStyle = $t.attr('data-object-fit');
			if (imgUrl) {
				$t.parent().css({
					'backgroundImage': 'url(' + imgUrl + ')',
					'backgroundSize': fitStyle
				}).addClass('fit-img');
			}
		});
	},

	getScrollbarSize: function getScrollbarSize() {
		var scrollbarSize = void 0;
		if (scrollbarSize === undefined) {
			var scrollDiv = document.createElement('div');
			scrollDiv.style.cssText = 'width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;';
			document.body.appendChild(scrollDiv);
			scrollbarSize = scrollDiv.offsetWidth - scrollDiv.clientWidth;
			document.body.removeChild(scrollDiv);
		}
		return scrollbarSize;
	},

	getScreenSize: function getScreenSize() {
		var screenSize = window.getComputedStyle(document.querySelector('body'), ':after').getPropertyValue('content');
		screenSize = parseInt(screenSize.match(/\d+/));
		return screenSize;
	}
};

$(function () {
	myApp.init();
});

$(window).on('load', function () {
	myApp.initOnLoad();
});
//# sourceMappingURL=main.js.map
